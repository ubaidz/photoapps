<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, x-xsrf-token");
include 'konek.php';

$return = array(
	'status' => 0,
	'message' => 'email kosong',
);
if (isset($_POST['email']) && $_POST['email'] != '') {
	$images = str_replace('img/', '', $_POST['images']);
	$check = checkEmail($_POST['email']);
	if ($check) {
		require_once 'mailchimp-mandrill-api/src/Mandrill.php'; //Not required with Composer
		$mandrill = new Mandrill('0BeNXahCZHBYGXGP92h5qQ');
		$sen_name = "KTB Truck Champaign 2019";
		$sen_email = "donotreply@wooz.in";
		$message = array();
		$to = array();
		$to[] = array(
			'email' => $_POST['email'],
		);
		$attachment = file_get_contents(realpath(dirname(__FILE__)) . '/img/' . $images);
		$attachment_encoded = base64_encode($attachment);
		$message['subject'] = 'Photobooth - KTB Truck Champaign 2019';
		$message['html'] = "KTB Truck Champaign 2019";
		$message['attachments'] = array(
			array(
				'content' => $attachment_encoded,
				'type' => "image/jpeg",
				'name' => $images,
			),
		);
		$message['from_email'] = $sen_email;
		$message['from_name'] = $sen_name;
		$message['to'] = $to;
		if (isset($to[0]['email']) && $to[0]['email'] !== "") {
			$sqlinsert = "insert into photos(account_id,places_id,photo_upload,email_id,name,status)
				        values( '1', '1', '" . $images . "','" . $_POST['email'] . "','" . $_POST['name'] . "',3)";
			$con->query($sqlinsert);
			$result = $mandrill->messages->send($message);
			$status = $result[0]['status'];
			$return = array(
				'status' => 1,
				'name' => $_POST['name'],
				'message' => $status,
			);
		} else {
			$return = array(
				'status' => 0,
				'message' => 'email kosong',
			);
		}
	} else {
		$return = array(
			'status' => 0,
			'message' => 'rfid kosong',
		);
	}
}
echo json_encode($return);

function checkEmail($email) {
	$find1 = strpos($email, '@');
	$find2 = strpos($email, '.');
	return ($find1 !== false && $find2 !== false && $find2 > $find1 ? true : false);
}

function checkdatarfid($rfid, $images) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_VERBOSE, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_URL, 'https://id.wooz.in/api/user');
	$post = array(
		"qrcode" => $rfid,
		"images" => $images,
	);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	$data['response'] = json_decode(curl_exec($ch));
	$data['httpcode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	// print_r($data);die;
	return $data;
}

function checkrfid($rfid) {
	$sqldevice = "SELECT * FROM users where rfid = '" . $rfid . "'";

	$querydevice = $con->query($sqldevice);
	$resultdevice = $querydevice->fetch_object();
	//print_r($data['httpcode']);die;
	return $resultdevice;
}

?>